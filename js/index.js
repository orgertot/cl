function initMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 11,
		center: {lat: 53.780501, lng: 9.980500 },
		mapTypeId: 'terrain',
		disableDefaultUI: true,
		styles: [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}]
	});

	// Define the LatLng coordinates for the polygon's path. https://codepen.io/jhawes/pen/ujdgK
	var triangleCoords = [
		new google.maps.LatLng(53.76369,9.9796),
		new google.maps.LatLng(53.75974,9.98325),
		new google.maps.LatLng(53.75409,9.98825),
		new google.maps.LatLng(53.75114,9.99043),
		new google.maps.LatLng(53.74804,9.99183),
		new google.maps.LatLng(53.74842,9.99823),
		new google.maps.LatLng(53.74778,10.00136),
		new google.maps.LatLng(53.74615,10.00563),
		new google.maps.LatLng(53.74556,10.00789),
		new google.maps.LatLng(53.74503,10.01066),
		new google.maps.LatLng(53.74736,10.0153),
		new google.maps.LatLng(53.7498,10.00894),
		new google.maps.LatLng(53.75204,10.00275),
		new google.maps.LatLng(53.75719,10.00764),
		new google.maps.LatLng(53.7587,10.01226),
		new google.maps.LatLng(53.75819,10.0134),
		new google.maps.LatLng(53.75962,10.02167),
		new google.maps.LatLng(53.75886,10.02616),
		new google.maps.LatLng(53.75825,10.03083),
		new google.maps.LatLng(53.75646,10.03891),
		new google.maps.LatLng(53.75692,10.03918),
		new google.maps.LatLng(53.76594,10.02529),
		new google.maps.LatLng(53.77025,10.00622),
		new google.maps.LatLng(53.77117,10.00001),
		new google.maps.LatLng(53.7717,9.99363),
		new google.maps.LatLng(53.77189,9.99322),
		new google.maps.LatLng(53.77211,9.99279),
		new google.maps.LatLng(53.77266,9.99214),
		new google.maps.LatLng(53.77685,9.98752),
		new google.maps.LatLng(53.77725,9.98518),
		new google.maps.LatLng(53.77673,9.97855),
		new google.maps.LatLng(53.7756,9.97869),
		new google.maps.LatLng(53.77556,9.97926),
		new google.maps.LatLng(53.77504,9.97975),
		new google.maps.LatLng(53.77332,9.97975),
		new google.maps.LatLng(53.77333,9.97879),
		new google.maps.LatLng(53.77028,9.9795),
		new google.maps.LatLng(53.76586,9.97873),
		new google.maps.LatLng(53.76526,9.97882),
		new google.maps.LatLng(53.76462,9.97905)
	];

	// Construct the polygon.
	var bermudaTriangle = new google.maps.Polygon({
		paths: triangleCoords,
		strokeColor: '#000',
		strokeOpacity: 0.8,
		strokeWeight: 2,
		fillColor: '#000',
		fillOpacity: 0.3
	});
	bermudaTriangle.setMap(map);
}
function initCalender() {
	fetch("https://www.googleapis.com/calendar/v3/calendars/7rghjcapassld9kmtudf71k0ds@group.calendar.google.com/events?key=AIzaSyA-qUwcPtCRWAFi9-uvSI1pDWMY2JStV8U&timeMin="+ new Date().toISOString() +"&maxResults=3&singleEvents=true&orderBy=startTime")
	.then(function (resp) {
		return resp.json(); 
	})
	.then(function(data) {
		var calender = document.getElementById('calender');
		var months = ["Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"];
		var days = ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"];
		data.items.forEach(function(event) {
			var div = document.createElement("div");
			//Title
			var titlelabel = document.createElement("label");
			titlelabel.className = "title";
			titlelabel.innerHTML = event.summary;
			div.appendChild(titlelabel);
			//Date
			if(event.start.dateTime) {
				var datetimelabel = document.createElement("label");
				var startDate = new Date(event.start.dateTime);
				datetimelabel.innerHTML = days[startDate.getDay()] + ", " + startDate.getDate() + ". " + months[startDate.getMonth()] + ", " + startDate.getFullYear() + ", " + 
											(String(startDate.getHours()).length > 1 ? startDate.getHours() : "0" + startDate.getHours()) + ":" + (String(startDate.getMinutes()).length > 1 ? startDate.getMinutes() : "0" + startDate.getMinutes());
				if(event.end.dateTime && event.end.dateTime != event.start.dateTime) {
					var endDate = new Date(event.end.dateTime);
					datetimelabel.innerHTML += " - " + (String(endDate.getHours()).length > 1 ? endDate.getHours() : "0" + endDate.getHours()) + ":" + (String(endDate.getMinutes()).length > 1 ? endDate.getMinutes() : "0" + endDate.getMinutes());
				}
				div.appendChild(datetimelabel);
			} else {
				if(event.start.date) {
					var datetimelabel = document.createElement("label");
					var startDate = new Date(event.start.date);
					datetimelabel.innerHTML = days[startDate.getDay()] + ", " + startDate.getDate() + ". " + months[startDate.getMonth()] + ", " + startDate.getFullYear();
					div.appendChild(datetimelabel);
				} else {
					titlelabel.className += " nodate";
				}
			}		
			//Location
			if(event.location) {
				var locationlabel = document.createElement("label");
				locationlabel.innerHTML = event.location;
				div.appendChild(locationlabel);
			}
			//Create
			calender.appendChild(div);
		})
	})
	.catch(function(error) {
		console.error(error);
	});
}
initCalender();